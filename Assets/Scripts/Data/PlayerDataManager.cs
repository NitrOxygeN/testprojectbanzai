﻿using System;
using System.Collections;
using UnityEngine;
using Random = UnityEngine.Random;

public class PlayerDataManager : PersistentSingleton<PlayerDataManager>
{
    [NonSerialized]
    private DataStore Data;

    public int SelectedTankBody
    {
        get { return Data.SelectedTankBody; }
        set { Data.SelectedTankBody = value; }
    }

    public int SelectedTankWeapon
    {
        get { return Data.SelectedTankWeapon; }
        set { Data.SelectedTankWeapon = value; }
    }

    IEnumerator Start()
    {
        while (TankLibrary.Instance == null)
            yield return null;

        Data = new DataStore();
        Data.SelectedTankBody = PlayerPrefs.GetInt("body", 0);
        Data.SelectedTankWeapon = PlayerPrefs.GetInt("weapon", 0);
    }

    protected override void OnDestroy()
    {
        if (Instance == this)
        {
            SaveData();
        }

        base.OnDestroy();
    }

    public void SaveData()
    {
        PlayerPrefs.SetInt("body", Data.SelectedTankBody);
        PlayerPrefs.SetInt("weapon", Data.SelectedTankWeapon);
    }

    public bool IsInitialized()
    {
        return Data != null;
    }

    public TankBody GetCurrentTankBody()
    {
        return TankLibrary.Instance.GetTankBodyDataFromIndex(SelectedTankBody);
    }

    public TankWeapon GetCurrentTankWeapon()
    {
        return TankLibrary.Instance.GetTankWeaponDataFromIndex(SelectedTankWeapon);
    }

    public TankShell GetTankWeaponShell(TankWeapon weapon)
    {
        return TankLibrary.Instance.GetTankShellDataFromId(weapon.TankShellId);
    }

    public TankEnemy GetRandomEnemy()
    {
        int index = Random.Range(0, TankLibrary.Instance.EnemiesCount);
        return TankLibrary.Instance.GetTankEnemyDataFromIndex(index);
    }

    public TankWeapon SelectNextWeapon()
    {
        Data.SelectedTankWeapon = ++Data.SelectedTankWeapon % TankLibrary.Instance.WeaponsCount;
        return GetCurrentTankWeapon();
    }

    public TankWeapon SelectPreviousWeapon()
    {
        if (Data.SelectedTankWeapon == 0)
            Data.SelectedTankWeapon = TankLibrary.Instance.WeaponsCount;
        Data.SelectedTankWeapon = --Data.SelectedTankWeapon % TankLibrary.Instance.WeaponsCount;
        return GetCurrentTankWeapon();
    }
}

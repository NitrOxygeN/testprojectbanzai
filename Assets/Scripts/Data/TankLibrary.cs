﻿using UnityEngine;
using System;

[Serializable]
public class TankBody
{
    public string Id;
    public string Name;

    public uint Health;
    public float Defence;

    public float Speed;
    public float TurnRate;

    public Vector2 CollisionOffset;
    public Vector2 CollisionSize;

    public Sprite DisplaySprite;
}

[Serializable]
public class TankWeapon
{
    public string Id;
    public string Name;

    public string TankShellId;
    public float FireRate;
    public Vector2 FireRootOffset;

    public Sprite DisplaySprite;
}

[Serializable]
public class TankShell
{
    public string Id;
    public float Damage;
    public float Velocity;

    public Vector2 CollisionSize;

    public Sprite DisplaySprite;
}

[Serializable]
public class TankEnemy
{
    public string Id;
    public float Damage;
    public float Velocity;
    public uint Score;

    public uint Health;
    public float Defence;

    public Vector2 CollisionSize;

    public Sprite DisplaySprite;
}

public class TankLibrary : PersistentSingleton<TankLibrary>
{
    [SerializeField]
    private TankLibraryData data;

    public int BodiesCount { get { return data.TankBodies.Length; } }

    public int WeaponsCount { get { return data.TankWeapons.Length; } }

    public int EnemiesCount { get { return data.TankEnemies.Length; } }

    public TankBody GetTankBodyDataFromIndex(int index)
    {
        if (index < 0 || index >= data.TankBodies.Length)
        {
            index = 0;
        }
        return data.TankBodies[index];
    }

    public TankWeapon GetTankWeaponDataFromIndex(int index)
    {
        if (index < 0 || index >= data.TankWeapons.Length)
        {
            index = 0;
        }
        return data.TankWeapons[index];
    }

    public TankShell GetTankShellDataFromId(string id)
    {
        for (int i = 0; i < data.TankShells.Length; i++)
        {
            if (data.TankShells[i].Id == id)
                return data.TankShells[i];
        }
        return data.TankShells[0];
    }

    public TankEnemy GetTankEnemyDataFromIndex(int index)
    {
        if (index < 0 || index >= data.TankEnemies.Length)
        {
            index = 0;
        }
        return data.TankEnemies[index];
    }
}

[CreateAssetMenu(fileName = "TankLibraryData", menuName = "Serialize/TankLibraryData", order = 1)]
public class TankLibraryData : ScriptableObject
{
    public TankBody[] TankBodies;
    public TankWeapon[] TankWeapons;
    public TankShell[] TankShells;
    public TankEnemy[] TankEnemies;
}
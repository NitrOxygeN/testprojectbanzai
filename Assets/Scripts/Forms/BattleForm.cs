﻿using UnityEngine;
using UnityEngine.UI;

public class BattleForm : MonoBehaviour
{
    [SerializeField]
    private Image HealthFill;
    [SerializeField]
    private Text HealthValue;
    [SerializeField]
    private Text ScoreValue;

    void Start()
    {
        GameController.Instance.OnScoreAdded += OnScoreAdded;
        GameController.Instance.OnHealthChanged += OnHealthChanged;

        OnScoreAdded(0);
        OnHealthChanged(1f);
    }

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            OnQuitClicked();
        }
    }

    public void OnQuitClicked()
    {
        BattleUI.Instance.ShowAskPanel();
    }

    private void OnScoreAdded(uint score)
    {
        ScoreValue.text = "Total Score : " + score;
    }

    private void OnHealthChanged(float health)
    {
        HealthFill.fillAmount = health;
        HealthValue.text = health.ToString("P0");

        if (health <= 0f)
        {
            BattleUI.Instance.ShowDeathPanel();
        }
    }
}

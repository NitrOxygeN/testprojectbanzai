﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class BattleUI : Singleton<BattleUI>
{
    [SerializeField]
    private CanvasGroup BattlePanel;
    [SerializeField]
    private CanvasGroup AskPanel;
    [SerializeField]
    private CanvasGroup DeathPanel;

    private CanvasGroup CurrentPanel;

    void Start()
    {
        ShowDefaultPanel();
    }

    public void ShowPanel(CanvasGroup newPanel)
    {
        if (CurrentPanel != null)
        {
            CurrentPanel.gameObject.SetActive(false);
        }

        CurrentPanel = newPanel;
        if (CurrentPanel != null)
        {
            CurrentPanel.gameObject.SetActive(true);
        }
    }

    public void ShowDefaultPanel()
    {
        ShowPanel(BattlePanel);
        GameController.Instance.Pause(false);
    }

    public void ShowAskPanel()
    {
        ShowPanel(AskPanel);
        GameController.Instance.Pause(true);
    }

    public void ShowDeathPanel()
    {
        ShowPanel(DeathPanel);
        GameController.Instance.Pause(true);
    }

    public void OnQuitMainMenuClicked()
    {
        SceneManager.LoadScene("Main");
        Cursor.lockState = CursorLockMode.None;
    }

    public void OnRestartBattleClicked()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }
}

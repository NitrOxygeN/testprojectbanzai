﻿using UnityEngine;
using UnityEngine.UI;

public class DeathForm : MonoBehaviour
{
    [SerializeField]
    private Text ScoreValue;

    void OnEnable()
    {
        ScoreValue.text = "Your Score: " + GameController.Instance.TankScore;
    }
}

﻿using UnityEngine;
using UnityEngine.UI;

public class LobbyCustomization : MonoBehaviour
{
    [SerializeField]
    private Button OkButton;
    [SerializeField]
    private Button CancelButton;
    [SerializeField]
    private Text TankBodyName;
    [SerializeField]
    private GameObject TankBodyNextButton, TankBodyPreviousButton;
    [SerializeField]
    private Text TankWeaponName;
    [SerializeField]
    private GameObject TankWeaponNextButton, TankWeaponPreviousButton;
    [SerializeField]
    private Image TankBodyImage, TankWeaponImage;

    int CurrentBody = 0;
    int CurrentWeapon = 0;

    void OnEnable()
    {
        ResetSelections();
        UpdateSelection();
    }

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            OnBackClicked();
        }
    }

    public void OKButton()
    {
        PlayerDataManager dataManager = PlayerDataManager.Instance;
        if (dataManager != null)
        {
            dataManager.SelectedTankBody = CurrentBody;
            dataManager.SelectedTankWeapon = CurrentWeapon;
            dataManager.SaveData();
        }
        MainMenuUI.Instance.ShowDefaultPanel();
    }

    public void OnBackClicked()
    {
        MainMenuUI.Instance.ShowDefaultPanel();
    }

    public void ChangeBodyButton(int direction)
    {
        CurrentBody = Wrap(CurrentBody + direction, TankLibrary.Instance.BodiesCount);
        UpdateSelection();
    }

    public void ChangeWeaponButton(int direction)
    {
        CurrentWeapon = Wrap(CurrentWeapon + direction, TankLibrary.Instance.WeaponsCount);
        UpdateSelection();
    }

    private void UpdateSelection()
    {
        TankBody bodyData = TankLibrary.Instance.GetTankBodyDataFromIndex(CurrentBody);
        TankWeapon weaponData = TankLibrary.Instance.GetTankWeaponDataFromIndex(CurrentWeapon);
        TankBodyName.text = bodyData.Name;
        TankBodyImage.sprite = bodyData.DisplaySprite;
        TankWeaponName.text = weaponData.Name;
        TankWeaponImage.sprite = weaponData.DisplaySprite;
    }

    private void ResetSelections()
    {
        PlayerDataManager dataManager = PlayerDataManager.Instance;
        if (dataManager != null)
        {
            CurrentBody = dataManager.SelectedTankBody;
            CurrentWeapon = dataManager.SelectedTankWeapon;
        }
    }

    private int Wrap(int index, int arraySize)
    {
        return index % arraySize;
    }
}

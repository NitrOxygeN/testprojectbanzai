﻿using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MainMenuUI : Singleton<MainMenuUI>
{
    [SerializeField]
    private CanvasGroup DefaultPanel;
    [SerializeField]
    private CanvasGroup CustomizePanel;
    [SerializeField]
    private CanvasGroup LoadingPanel;
    [SerializeField]
    private GameObject QuitButton;

    private CanvasGroup CurrentPanel;

    IEnumerator Start()
    {
        ShowPanel(LoadingPanel);

#if UNITY_EDITOR || UNITY_ANDROID || UNITY_IOS
        if (QuitButton != null)
        {
            QuitButton.SetActive(false);
        }
#endif

        while (!PlayerDataManager.Instance.IsInitialized())
            yield return null;

        ShowDefaultPanel();
    }
	
    public void ShowPanel(CanvasGroup newPanel)
    {
        if (CurrentPanel != null)
        {
            CurrentPanel.gameObject.SetActive(false);
        }

        CurrentPanel = newPanel;
        if (CurrentPanel != null)
        {
            CurrentPanel.gameObject.SetActive(true);
        }
    }

    public void ShowDefaultPanel()
    {
        ShowPanel(DefaultPanel);
    }

    public void ShowCustomizePanel()
    {
        ShowPanel(CustomizePanel);
    }

    public void OnQuitGameClicked()
    {
        Application.Quit();
    }

    public void OnStartGameClicked()
    {
        SceneManager.LoadScene("Battle");
        Cursor.lockState = CursorLockMode.Confined;
    }
}
﻿using UnityEngine;

public class Enemy : MonoBehaviour
{
    public SpriteRenderer EnemySprite;
    public SpriteRenderer HealthSprite;
    public BoxCollider2D Collider;

    [HideInInspector]
    public bool IsAlive = false;

    private TankEnemy tankEnemy;
    private float maxHealthScale = 45f;
    private float currentHealth = 0f;

    void Start ()
    {
        maxHealthScale = HealthSprite.transform.localScale.x;
        SetHealth();
    }
	
	void Update ()
	{
	    Vector2 tankDirection = GameController.Instance.TankLocation - transform.position;
	    EnemySprite.transform.eulerAngles = new Vector3(0f, 0f, Mathf.Atan2(tankDirection.y, tankDirection.x) * Mathf.Rad2Deg - 90f);
        transform.position += EnemySprite.transform.up * tankEnemy.Velocity * Time.deltaTime;
    }

    public void SetEnemy(TankEnemy enemy)
    {
        EnemySprite.sprite = enemy.DisplaySprite;
        Collider.size = enemy.CollisionSize;

        tankEnemy = enemy;
        IsAlive = true;
        currentHealth = enemy.Health;
        SetHealth();
    }

    private void Damage(Shell shell)
    {
        currentHealth -= GameController.Instance.GetDamage(shell.Damage, tankEnemy.Defence);
        SetHealth();
        IsAlive = currentHealth > 0f;
    }

    private void SetHealth()
    {
        HealthSprite.transform.localScale = new Vector3((currentHealth / tankEnemy.Health) * maxHealthScale, HealthSprite.transform.localScale.y, HealthSprite.transform.localScale.z);
    }

    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.CompareTag("Shell"))
        {
            Damage(other.gameObject.GetComponent<Shell>());
            if (!IsAlive)
            {
                gameObject.SetActive(false);
                GameController.Instance.AddTankScore(tankEnemy.Score);
            }

            Destroy(other.gameObject);
        }

        if (other.gameObject.CompareTag("Tank"))
        {
            IsAlive = false;
            gameObject.SetActive(false);
            GameController.Instance.DamageTank(tankEnemy.Damage);
        }
    }
}

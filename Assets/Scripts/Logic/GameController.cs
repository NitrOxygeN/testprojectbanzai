﻿using UnityEngine;

public class GameController : Singleton<GameController>
{
    [SerializeField]
    private GameObject tankPrefab;
    [SerializeField]
    private GameObject enemyPrefab;
    [SerializeField]
    private SmoothCamera2D cameraFollow;
    [SerializeField]
    private int maxEnemies;
    [SerializeField]
    private float minNextEnemyTime;
    [SerializeField]
    private float maxNextEnemyTime;
    [SerializeField]
    private float minDifficultCoef;
    [SerializeField]
    private float maxDiffucultTime;

    private Tank tank;
    private Enemy[] enemies;

    private float startTime = 0f;

    public delegate void ScoreAdded(uint score);
    public delegate void HealthChanged(float health);
    public event ScoreAdded OnScoreAdded;
    public event HealthChanged OnHealthChanged;

    public Vector3 TankLocation { get { return tank.transform.position; } }
    public uint TankScore { get { return tank.Score; } }

    void Start ()
	{
	    tank = Instantiate(tankPrefab, Vector3.zero, Quaternion.identity).GetComponent<Tank>();
        tank.SetTank(PlayerDataManager.Instance.GetCurrentTankBody(), PlayerDataManager.Instance.GetCurrentTankWeapon());
	    cameraFollow.Target = tank.transform;

        enemies = new Enemy[maxEnemies];
        for (int i = 0; i < maxEnemies; i++)
	    {
            enemies[i] = Instantiate(enemyPrefab, Vector3.zero, Quaternion.identity).GetComponent<Enemy>();
            enemies[i].gameObject.SetActive(false);
        }

	    startTime = Time.time;
	}

    private float nextEnemyTime = 0f;
	void Update ()
	{
	    if (Time.time > nextEnemyTime)
	    {
	        for (int i = 0; i < maxEnemies; i++)
	        {
	            if (!enemies[i].IsAlive)
	            {
                    enemies[i].SetEnemy(PlayerDataManager.Instance.GetRandomEnemy());
	                enemies[i].transform.position = GetRandomCircleLocation(20f);
	                enemies[i].gameObject.SetActive(true);
                    break;
	            }
	        }

	        float difficultCoef = minDifficultCoef + Mathf.Max(0f, (maxDiffucultTime - (Time.time - startTime)) / maxDiffucultTime) * (1f - minDifficultCoef);
	        nextEnemyTime = Time.time + Random.Range(minNextEnemyTime, maxNextEnemyTime) * difficultCoef;
	    }
	}

    public void Pause(bool isPaused)
    {
        Time.timeScale = isPaused ? 0f : 1f;
        tank.enabled = !isPaused;
    }

    public void AddTankScore(uint score)
    {
        tank.AddScore(score);
        if (OnScoreAdded != null)
            OnScoreAdded(tank.Score);
    }

    public void DamageTank(float damage)
    {
        tank.Damage(damage);
        if (OnHealthChanged != null)
            OnHealthChanged(tank.Health);
    }

    public float GetDamage(float baseDamage, float defence)
    {
        return baseDamage * (1f - defence);
    }

    private Vector2 GetRandomCircleLocation(float radius)
    {
        float angle = Random.Range(0, 10) * 36f * 3.14f / 180.0f;
        float x = TankLocation.x + radius * Mathf.Cos(angle);
        float y = TankLocation.y + radius * Mathf.Sin(angle);
        return new Vector2(x, y);
    }
}

﻿using UnityEngine;

public class Shell : MonoBehaviour
{
    public SpriteRenderer ShellSprite;
    public BoxCollider2D Collider;

    private TankShell tankShell;

    public float Damage { get { return tankShell.Damage; } }

	void Start ()
	{
		Destroy(this, 10f);
	}
	
	void Update ()
	{
	    transform.position += transform.up * tankShell.Velocity * Time.deltaTime;
    }

    public void SetShell(TankShell shell)
    {
        ShellSprite.sprite = shell.DisplaySprite;
        Collider.size = shell.CollisionSize;

        tankShell = shell;
    }
}

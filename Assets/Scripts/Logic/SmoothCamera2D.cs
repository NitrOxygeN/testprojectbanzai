﻿using UnityEngine;

public class SmoothCamera2D : MonoBehaviour
{
    public Transform Target;
    public float DampTime = 0.15f;
    private Camera camera;
    private Vector3 velocity = Vector3.zero;

    void Start()
    {
        camera = GetComponent<Camera>();
        if (camera == null)
            enabled = false;
    }

    void Update()
    {
        if (Target)
        {
            Vector3 point = camera.WorldToViewportPoint(Target.position);
            Vector3 delta = Target.position - camera.ViewportToWorldPoint(new Vector3(0.5f, 0.5f, point.z));
            Vector3 destination = transform.position + delta;
            transform.position = Vector3.SmoothDamp(transform.position, destination, ref velocity, DampTime);
        }
    }
}
﻿using UnityEngine;

public class Tank : MonoBehaviour
{
    public SpriteRenderer BodySprite;
    public SpriteRenderer WeaponSprite;

    public BoxCollider2D Collider;
    public Rigidbody2D RigidBody;
    public Transform FireRoot;

    public GameObject ShellPrefab;

    private Vector3 halfScreen;

    private TankBody tankBody;
    private TankWeapon tankWeapon;
    private TankShell tankShell;

    private float currentHealth = 0f;
    private uint currentScore = 0;

    public float Health { get { return currentHealth / tankBody.Health; } }
    public uint Score { get { return currentScore; } }

	void Start ()
	{
	    halfScreen = new Vector3(Screen.width / 2f, Screen.height / 2f);
    }
	
	void Update ()
	{
	    if (Input.GetKeyDown(KeyCode.E))
	    {
	        UpdateWeapon(PlayerDataManager.Instance.SelectNextWeapon());
	    }
	    if (Input.GetKeyDown(KeyCode.Q))
	    {
	        UpdateWeapon(PlayerDataManager.Instance.SelectPreviousWeapon());
        }
	    if (Input.GetMouseButtonDown(0))
	    {
	        Fire();
	    }

	    Vector2 mouseDirection = Input.mousePosition - halfScreen;
	    WeaponSprite.transform.eulerAngles = new Vector3(0f, 0f, Mathf.Atan2(mouseDirection.y, mouseDirection.x) * Mathf.Rad2Deg - 90f);

        float moveValue = Input.GetAxis("Vertical");
        float turnValue = -Input.GetAxis("Horizontal");

	    float sign = Mathf.Abs(moveValue) > 0f ? Mathf.Sign(moveValue) : 1f;
	    transform.Rotate(new Vector3(0f, 0f, turnValue * sign * tankBody.TurnRate * Time.deltaTime));
	    transform.position += transform.up * moveValue * tankBody.Speed * Time.deltaTime;
    }

    public void SetTank(TankBody body, TankWeapon weapon)
    {
        UpdateWeapon(weapon);

        BodySprite.sprite = body.DisplaySprite;
        Collider.offset = body.CollisionOffset;
        Collider.size = body.CollisionSize;

        tankBody = body;
        currentHealth = body.Health;
    }

    public void AddScore(uint score)
    {
        currentScore += score;
    }

    public void Damage(float damage)
    {
        currentHealth -= GameController.Instance.GetDamage(damage, tankBody.Defence);
    }

    private void UpdateWeapon(TankWeapon weapon)
    {
        WeaponSprite.sprite = weapon.DisplaySprite;
        FireRoot.localPosition = weapon.FireRootOffset;

        tankWeapon = weapon;
        tankShell = PlayerDataManager.Instance.GetTankWeaponShell(weapon);
    }

    private float nextFireTime = 0f;
    private void Fire()
    {
        if (Time.time > nextFireTime)
        {
            nextFireTime = Time.time + 1f / tankWeapon.FireRate;
            GameObject shell = Instantiate(ShellPrefab, FireRoot.position, FireRoot.rotation);
            shell.GetComponent<Shell>().SetShell(tankShell);
            Destroy(shell, 5f);
        }
    }
}
